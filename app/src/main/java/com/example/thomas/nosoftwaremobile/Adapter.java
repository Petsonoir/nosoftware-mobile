package com.example.thomas.nosoftwaremobile;

/**
 * Created by Thomas on 18/01/2018.
 */

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Arrays;
import java.util.List;

// Classe chargant et affichant les données contenues dans notre RecyclerView
public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    // On créé une liste de données par pair Nom/Rôle des employées de NOSoftware
    private final List<Pair<String, String>> employees = Arrays.asList(
            Pair.create("Florian Guinard", "Fondateur et Chef de projet chez NOSoftware."),
            Pair.create("Thomas Pontoizeau", "Fondateur et Scrum Master chez NOSoftware."),
            Pair.create("Nicolas Chauvigné", "Fondateur et Lead dev web chez NOSoftware."),
            Pair.create("Renaud Goislot", "Fondateur et Lead dev logiciel chez NOSoftware."),
            Pair.create("Merlin Loison", "Merlin chez NOSoftware."),
            Pair.create("Stanislas Molveau", "Web designer chez NOSoftware."),
            Pair.create("Khurram Rajput", "Développeur et testeur chez NOSoftware.")
    );
// Méthodes permettant de charger les données du RecyclerView
    @Override
    public int getItemCount() {
        return employees.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_cell, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pair<String, String> pair = employees.get(position);
        holder.display(pair);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView role;
        private final ImageView image;


        private Pair<String, String> currentPair;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = ((TextView) itemView.findViewById(R.id.name));
            role = ((TextView) itemView.findViewById(R.id.role));
            image = (ImageView) itemView.findViewById(R.id.image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(currentPair.first)
                            .setMessage(currentPair.second)
                            .show();
                }
            });
        }

        // Affiche les données contenues dans la liste
        public void display(Pair<String, String> pair) {
            currentPair = pair;
            // Switch statement affichant les photos existantes pour le moment
            switch (pair.first) {
                case "Florian Guinard":
                    image.setImageResource(R.drawable.flo);
                    break;
                case "Thomas Pontoizeau":
                    image.setImageResource(R.drawable.tho);
                    break;
                case "Renaud Goislot":
                    image.setImageResource(R.drawable.ren);
                    break;
                default:
                    image.setImageResource(R.drawable.random);
                    break;
            }
            name.setText(pair.first);
            role.setText(pair.second);
        }
    }

}