package com.example.thomas.nosoftwaremobile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;
public class AsyncBigCalculActivity extends Activity {

    private ProgressBar mProgressBar;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // On récupère les composants de notre layout
        mProgressBar = (ProgressBar) findViewById(R.id.pBAsync);
                BigCalcul calcul=new BigCalcul();
                calcul.execute();
    }

    // Classe AsyncTask chargant la localisation de la position NOSoftware
    private class BigCalcul extends AsyncTask<Void, Integer, Void>
    {
        @Override
        // Avant exécution de l'AsyncTask
        protected void onPreExecute() {
            super.onPreExecute();
            // Affiche un message prévenant que la localisation est en cours de chargement
            Toast.makeText(getApplicationContext(), "Localisation en cours ...", Toast.LENGTH_LONG).show();
        }

        @Override
        // Permet de charger la barre de chargement
        protected void onProgressUpdate(Integer... values){
            super.onProgressUpdate(values);
            // Mise à jour de la ProgressBar
            mProgressBar.setProgress(values[0]);
        }

        @Override
        // Permet de charger la barre de chargement
        protected Void doInBackground(Void... arg0) {

            int progress;
            for (progress=0;progress<=100000;progress++)
            {

                //la méthode publishProgress met à jour l'interface en invoquant la méthode onProgressUpdate
                publishProgress(progress);
                progress++;
            }
            return null;
        }

        @Override
        //Se déclenche une fois l'exécution de l'AsyncTask terminée
        protected void onPostExecute(Void result) {
            // Affiche un message prévenant que la localisation est terminée
            Toast.makeText(getApplicationContext(), "Localisation terminée !", Toast.LENGTH_LONG).show();
            // Lance l'activité MapsActivity
            startActivity(new Intent(AsyncBigCalculActivity.this, MapsActivity.class));
            finish();
        }
    }
}

