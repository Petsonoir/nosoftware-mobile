package com.example.thomas.nosoftwaremobile;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

//Activité principale: page d'accueil de l'application
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Les textes et images cachés
        final ImageView esaip = (ImageView) findViewById(R.id.esaip);
        final TextView Pavé = (TextView) findViewById(R.id.Pavé);

        // Button - Affiche un résumé de l'activité NOSoftware
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On rend visible le texte résumant l'activité NOSoftware
                Pavé.setVisibility((Pavé.getVisibility() == View.VISIBLE)
                        ? View.GONE : View.VISIBLE);

            }
        });

        // Bouton 1 - Affiche le logo ESAIP Shop
        Button bouton1 = (Button) findViewById(R.id.bouton1);
        bouton1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On rend visible l'image cliquable ouvrant un lien sur navigateur
                esaip.setVisibility((esaip.getVisibility() == View.VISIBLE)
                        ? View.GONE : View.VISIBLE);

            }
        });

        // Bouton 2 - Plus d'informations sur l'équipe de NOSoftware
        Button bouton2 = (Button)findViewById(R.id.bouton2);
        bouton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On démarre l'activité TeamActivity.java
                startActivity(new Intent(MainActivity.this, TeamActivity.class));
            }
        });


        // Bouton 3 - Ouvre la page permettant de contacter NOSoftware par divers moyens
        Button bouton3 = (Button)findViewById(R.id.button3);
        bouton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On démarre l'activité ContactActivity.java
                startActivity(new Intent(MainActivity.this, ContactActivity.class));
            }
        });



        // Redirige vers le site NOSoftware - ouvre un navigateur
        esaip.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1. Appeler une URL web
                String url = "https://nosoftware.ovh/fr_FR/";
                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                startActivity(intent);
            }
        });
    }

    // Si le bouton retour du téléphone est pressé on finit l'activité
    public void onBackPressed(){
        finish();
    }


}