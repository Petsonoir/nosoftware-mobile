package com.example.thomas.nosoftwaremobile;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

//Activité secondaire: page de présentation de l'équipe NOSoftware
public class TeamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_team);

        // On créé notre RecyclerView
        final RecyclerView rv = (RecyclerView) findViewById(R.id.list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new Adapter());

    }

    // Si le bouton retour du téléphone est pressé on finit l'activité
    public void onBackPressed(){
        finish();
    }

}
