package com.example.thomas.nosoftwaremobile;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

// Activité secondaire: page de contact de l'équipe NOSoftware
public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //Bouton "Retour" permettant de revenir à l'application principale
        Button boutonannuler = (Button)findViewById(R.id.boutonannuler);
        //Icône de mail permettant l'envoi de mail
        ImageButton imageMail = (ImageButton)findViewById(R.id.imageMail);
        //Icône de GoogleMap permettant l'affichage de la position de NOSoftware
        ImageButton imageMaps= (ImageButton)findViewById(R.id.imageMaps);
        //Icône d'appel permettant l'appel du numéro NOSoftware
        ImageButton imagePhone= (ImageButton)findViewById(R.id.imagePhone);
        //Icône de Facebook permettant d'ouvrir la page Facebook de NOSoftware
        ImageButton imageFacebook= (ImageButton)findViewById(R.id.imageFacebook);
        //Icône de Twitter permettant d'ouvrir la page Twitter de NOSoftware
        ImageButton imageTwitter= (ImageButton)findViewById(R.id.imageTwitter);
        //Icône de Pinterest permettant d'ouvrir la page Pinterest de NOSoftware
        ImageButton imagePinterest= (ImageButton)findViewById(R.id.imagePinterest);

        imageMail.setOnClickListener(new View.OnClickListener() {         // BOUTON ENVOYER - Envoi d'un mail via une application au choix de l'utilisateur

            public void onClick(View v) {

            try {

                String[] to = {"contact@nosoftware.ovh"};           // BOUTON ENVOYER - Construction de l'intent pour l'envoi des données vers l'application de mail
                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to);

                startActivity(Intent.createChooser(emailIntent, "Envoi du mail"));           // BOUTON ENVOYER - Démarrage de l'activité

            }

            catch (android.content.ActivityNotFoundException ex) {

                Toast.makeText(ContactActivity.this, "Aucun client mail n'est disponible", Toast.LENGTH_SHORT).show();

            }

            }

        });

        boutonannuler.setOnClickListener(new View.OnClickListener() {           // BOUTON ANNULER - Retour à l'activité principale

            public void onClick(View v) {

                startActivity(new Intent(ContactActivity.this, MainActivity.class));
                finish();

            }

        });


        imagePhone.setOnClickListener(new View.OnClickListener() {           // BOUTON ANNULER - Retour à l'activité principale

            public void onClick(View v) {

                Intent appel = new Intent(Intent.ACTION_DIAL);
                appel.setData(Uri.parse("tel:+33632246592"));
                startActivity(appel);

            }

        });

        // Bouton 2 - Google map
        imageMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactActivity.this, AsyncBigCalculActivity.class));
            }
        });


        imageFacebook.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            try{

                getPackageManager().getPackageInfo("com.facebook.katana", 0);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=https://www.facebook.com/NOSoftware"));
                startActivity(intent);

            }

            catch (Exception e) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/NOSoftware"));
                startActivity(intent);

            }

            }
        });

        imageTwitter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "https://twitter.com/NOS_contact";
                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                startActivity(intent);

            }
        });

        imagePinterest.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "https://www.pinterest.fr/nosoftware/";
                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                startActivity(intent);

            }
        });

    }

    public void onBackPressed(){
        finish();
    }

}