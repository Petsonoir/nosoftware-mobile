package com.example.thomas.nosoftwaremobile;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Florian on 25/01/2018.
 */

@RunWith(RobolectricTestRunner.class)
@Config(emulateSdk=18)

public class UnitTestAdapter {

    private Adapter adapter;

    @Before
    public void setUp() {
        adapter = new Adapter();
    }

    @Test
    public void nomTest() {
        assertEquals(6, adapter.getItemCount());
    }
}