T�ches effectu�es:

	Florian:
		Mise en place des tests unitaires
		Mise en place des intents (Facebook, Twitter, Pinterest, Mail, T�l�phone)
		Cr�ation et test des fonctionnalit�s de contact Facebook, Twitter, Pinterest, Mail et T�l�hone


	Thomas:
		Cr�ation du Git
		Mise en place de l'API Google Map
		Mise en place de l'AsyncTask
		Mise en place de la Recycler View


Activit�:
	Adapter.java : Thomas
	AsyncBigCalculActivity.java: Thomas
	ContactActivity.java: Florian
	MainActivity.java: Florian/Thomas
	MapsActivity.java: Thomas
	TeamActivity.java: Thomas

Layout:
	activity_contact.xml: Florian
	actitivy_main.xml: Florian/Thomas
	activity_maps.xml: Thomas
	activity_team.xml: Thomas
	content_team.xml: Thomas
	list_cell.xml: Thomas
	main.xml: Thomas



API 22: Android 5.1 (Lollipop)
Android 2.3.3
26 janvier 23h59


By Thomas P. and Florian G.
NOSoftware